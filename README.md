This is a clone of the official **Ramses** repository, that is actually using **Git** for version control. 
The multiple directories (present for historical reasons) have been remapped as follows:

* *trunk/ramses* of old branch *master* to new branch *trunk/master*
* *trunk/ramses* of old branch *open_acc* to new branch *trunk/open_acc*
* *tags/ramses_hp2c* (same in both old branches) to new branch *tags/hp2c*
* *branches/ramses_gpu* (same in both old branches) to new branch *branches/gpu*
* *branches/ramses_omp* (same in both old branches) to new branch *branches/omp*

To download a version of the code, clone its branch:
```
$ git clone -b <branch> https://bitbucket.org/gferrand/ramses-git.git
```
To switch to another version, check out its branch:
```
$ git checkout -b <branch> origin/<branch>
```